//! Types representing a theme.

#![allow(missing_docs)] // TODO

use crate::serializer::serialize;
use serde::{Deserialize, Serialize};

pub mod action_sheet;
pub mod chat;
pub mod chat_list;
mod color;
mod context_menu;
mod fill_foreground;
mod gradient;
mod intro;
pub mod list;
pub mod notification;
mod passcode;
mod reference_theme;
pub mod root;
mod status_bar;
mod variable_color;

pub use {
    action_sheet::ActionSheet,
    chat::Chat,
    chat_list::ChatList,
    color::Color,
    context_menu::ContextMenu,
    fill_foreground::{FillForeground, FillStrokeForeground},
    gradient::Gradient,
    intro::Intro,
    list::List,
    notification::Notification,
    passcode::Passcode,
    reference_theme::ReferenceTheme,
    root::Root,
    status_bar::StatusBar,
    variable_color::VariableColor,
};

/// Represents a theme.
#[derive(Debug, PartialEq, Eq, Clone, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Theme {
    pub name: String,
    pub based_on: ReferenceTheme,
    pub dark: bool,
    pub intro: Intro,
    pub passcode: Passcode,
    pub root: Root,
    pub list: List,
    pub chat_list: ChatList,
    pub chat: Chat,
    pub action_sheet: ActionSheet,
    pub context_menu: ContextMenu,
    pub notification: Notification,
}

impl Theme {
    pub fn to_string(&self) -> String {
        serialize(&self)
    }
}
