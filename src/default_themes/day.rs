use crate::theme::{
    action_sheet::{self, ActionSheet},
    chat::{
        input_panel::{InputPanel, MediaRecordControl},
        message::{
            bubble::{Bubble, Components},
            parted_colors::{PartedColors, Polls},
            Message,
        },
        service_message::{self, ServiceMessage},
        Chat, HistoryNav, InputButtonPanel, InputMediaPanel, Wallpaper,
    },
    chat_list::{ArchiveAvatarColors, ChatList},
    list::{DisclosureActions, FreeInputField, List, Switch},
    notification::{
        expanded::{self, Expanded},
        Notification,
    },
    root::{self, Keyboard, Root, SearchBar, TabBar},
    Color, ContextMenu, FillForeground, FillStrokeForeground, Gradient, Intro,
    Passcode, ReferenceTheme, StatusBar, Theme, VariableColor,
};

const DESTRUCTIVE_COLOR: Color = Color::new(0xff, 0x3b, 0x30, 0xff);
const CONSTRUCTIVE_COLOR: Color = Color::new(0x00, 0xc9, 0x00, 0xff);
const SECRET_COLOR: Color = Color::new(0x00, 0xb1, 0x2c, 0xff);

/// Generates a Day theme.
pub fn day(mut accent_color: Color, service_background_color: Color) -> Theme {
    let is_light = accent_color.lightness() > 0.705;

    let outgoing_primary_text =
        if is_light { Color::BLACK } else { Color::WHITE };
    let outgoing_secondary_text = if is_light {
        Color::new(0x00, 0x00, 0x00, 140)
    } else {
        Color::new(0xff, 0xff, 0xff, 0xa5)
    };
    let outgoing_link = outgoing_primary_text;
    let outgoing_check = outgoing_primary_text;
    let outgoing_bubble_background = accent_color;
    let outgoing_bubble_stroke = if is_light && accent_color == Color::WHITE {
        Color::new(0xc8, 0xc7, 0xcc, 0xff)
    } else {
        outgoing_bubble_background
    };
    let outgoing_selection = if is_light {
        let (hue, saturation, brightness) = accent_color.hsb();
        let saturation = (saturation * 1.1).min(1.0);
        let brightness = brightness.min(0.6);

        accent_color = Color::from_hsb(hue, saturation, brightness, 255);
        accent_color
    } else {
        Color::WHITE
    };

    Theme {
        name: "Day".to_string(),
        based_on: ReferenceTheme::Day,
        dark: false,
        intro: Intro {
            status_bar: StatusBar::Black,
            start_button: Color::new(0x2c, 0xa5, 0xe0, 0xff),
            dot: Color::new(0xd9, 0xd9, 0xd9, 0xff),
        },
        passcode: Passcode {
            bg: Gradient {
                top: Color::new(0x46, 0x73, 0x9e, 0xff),
                bottom: Color::new(0x2a, 0x59, 0x82, 0xff),
            },
            button: Color::CLEAR,
        },
        root: Root {
            status_bar: StatusBar::Black,
            tab_bar: TabBar {
                background: Color::new(0xf7, 0xf7, 0xf7, 0xff),
                separator: Color::new(0xa3, 0xa3, 0xa3, 0xff),
                icon: Color::new(0x95, 0x95, 0x95, 0xff),
                selected_icon: accent_color,
                text: Color::new(0x95, 0x95, 0x95, 0xff),
                selected_text: accent_color,
                badge_background: Color::new(0xff, 0x3b, 0x30, 0xff),
                badge_stroke: Color::new(0xff, 0x3b, 0x30, 0xff),
                badge_text: Color::WHITE,
            },
            nav_bar: root::NavBar {
                button: accent_color,
                disabled_button: Color::new(0xd0, 0xd0, 0xd0, 0xff),
                primary_text: Color::BLACK,
                secondary_text: Color::new(0x78, 0x78, 0x78, 0xff),
                control: Color::new(0x7e, 0x87, 0x91, 0xff),
                accent_text: accent_color,
                background: Color::new(0xf7, 0xf7, 0xf7, 0xff),
                separator: Color::new(0xb1, 0xb1, 0xb1, 0xff),
                badge_fill: Color::new(0xff, 0x3b, 0x30, 0xff),
                badge_stroke: Color::new(0xff, 0x3b, 0x30, 0xff),
                badge_text: Color::WHITE,
            },
            search_bar: SearchBar {
                background: Color::WHITE,
                accent: accent_color,
                input_fill: Color::new(0xe9, 0xe9, 0xe9, 0xff),
                input_text: Color::BLACK,
                input_placeholder_text: Color::new(0x8e, 0x8e, 0x93, 0xff),
                input_icon: Color::new(0x8e, 0x8e, 0x93, 0xff),
                input_clear_button: Color::new(0x7b, 0x7b, 0x81, 0xff),
                separator: Color::new(0xb1, 0xb1, 0xb1, 0xff),
            },
            keyboard: Keyboard::Light,
        },
        list: List {
            blocks_bg: Color::new(0xef, 0xef, 0xf4, 0xff),
            plain_bg: Color::WHITE,
            primary_text: Color::BLACK,
            secondary_text: Color::new(0x8e, 0x8e, 0x93, 0xff),
            disabled_text: Color::new(0x8e, 0x8e, 0x93, 0xff),
            accent: accent_color,
            highlighted: SECRET_COLOR,
            destructive: DESTRUCTIVE_COLOR,
            placeholder_text: Color::new(0xc8, 0xc8, 0xce, 0xff),
            item_blocks_bg: Color::WHITE,
            item_highlighted_bg: Color::new(0xd9, 0xd9, 0xd9, 0xff),
            blocks_separator: Color::new(0xc8, 0xc7, 0xcc, 0xff),
            plain_separator: Color::new(0xc8, 0xc7, 0xcc, 0xff),
            disclosure_arrow: Color::new(0xba, 0xb9, 0xbe, 0xff),
            section_header_text: Color::new(0x6d, 0x6d, 0x72, 0xff),
            free_text: Color::new(0x6d, 0x6d, 0x72, 0xff),
            free_text_error: Color::new(0xcf, 0x30, 0x30, 0xff),
            free_text_success: Color::new(0x26, 0x97, 0x2c, 0xff),
            free_mono_icon: Color::new(0x7e, 0x7e, 0x87, 0xff),
            switch: Switch {
                frame: Color::new(0xe0, 0xe0, 0xe0, 0xff),
                handle: Color::WHITE,
                content: Color::new(0x77, 0xd5, 0x72, 0xff),
                positive: CONSTRUCTIVE_COLOR,
                negative: DESTRUCTIVE_COLOR,
            },
            disclosure_actions: DisclosureActions {
                neutral1: FillForeground {
                    bg: Color::new(0x48, 0x92, 0xf2, 0xff),
                    fg: Color::WHITE,
                },
                neutral2: FillForeground {
                    bg: Color::new(0xf0, 0x9a, 0x37, 0xff),
                    fg: Color::WHITE,
                },
                destructive: FillForeground {
                    bg: Color::new(0xff, 0x38, 0x24, 0xff),
                    fg: Color::WHITE,
                },
                constructive: FillForeground {
                    bg: CONSTRUCTIVE_COLOR,
                    fg: Color::WHITE,
                },
                accent: FillForeground {
                    bg: accent_color,
                    fg: Color::WHITE,
                },
                warning: FillForeground {
                    bg: Color::new(0xff, 0x95, 0x00, 0xff),
                    fg: Color::WHITE,
                },
                inactive: FillForeground {
                    bg: Color::new(0xbc, 0xbc, 0xc3, 0xff),
                    fg: Color::WHITE,
                },
            },
            check: FillStrokeForeground {
                bg: accent_color,
                stroke: Color::new(0xc7, 0xc7, 0xcc, 0xff),
                fg: Color::WHITE,
            },
            control_secondary: Color::new(0xde, 0xde, 0xde, 0xff),
            free_input_field: FreeInputField {
                bg: Color::new(0xd6, 0xd6, 0xdc, 0xff),
                stroke: Color::new(0xd6, 0xd6, 0xdc, 0xff),
                placeholder: Color::new(0x96, 0x97, 0x9d, 0xff),
                primary: Color::BLACK,
                control: Color::new(0x96, 0x97, 0x9d, 0xff),
            },
            media_placeholder: Color::new(0xe4, 0xe4, 0xe4, 0xff),
            scroll_indicator: Color::new(0, 0, 0, 0x4c),
            page_indicator_inactive: Color::new(0xe3, 0xe3, 0xe7, 0xff),
            input_clear_button: Color::new(0xcc, 0xcc, 0xcc, 0xff),
        },
        chat_list: ChatList {
            bg: Color::WHITE,
            item_separator: Color::new(0xc8, 0xc7, 0xcc, 0xff),
            item_bg: Color::WHITE,
            pinned_item_bg: Color::new(0xf7, 0xf7, 0xf7, 0xff),
            item_highlighted_bg: Color::new(0xd9, 0xd9, 0xd9, 0xff),
            item_selected_bg: Color::new(0xe9, 0xf0, 0xfa, 0xff),
            title: Color::BLACK,
            secret_title: SECRET_COLOR,
            date_text: Color::new(0x8e, 0x8e, 0x93, 0xff),
            author_name: Color::BLACK,
            message_text: Color::new(0x8e, 0x8e, 0x93, 0xff),
            message_draft_text: Color::new(0xdd, 0x4b, 0x39, 0xff),
            checkmark: accent_color,
            pending_indicator: Color::new(0x8e, 0x8e, 0x93, 0xff),
            failed_fill: DESTRUCTIVE_COLOR,
            failed_fg: Color::WHITE,
            mute_icon: Color::new(0xa7, 0xa7, 0xad, 0xff),
            unread_badge_active_bg: accent_color,
            unread_badge_active_text: Color::WHITE,
            unread_badge_inactive_bg: Color::new(0xb6, 0xb6, 0xbb, 0xff),
            unread_badge_inactive_text: Color::WHITE,
            pinned_badge: Color::new(0xb6, 0xb6, 0xbb, 0xff),
            pinned_search_bar: Color::new(0xe5, 0xe5, 0xe5, 0xff),
            regular_search_bar: Color::new(0xe9, 0xe9, 0xe9, 0xff),
            section_header_bg: Color::new(0xf7, 0xf7, 0xf7, 0xff),
            section_header_text: Color::new(0x8e, 0x8e, 0x93, 0xff),
            verified_icon_bg: accent_color,
            verified_icon_fg: Color::WHITE,
            secret_icon: SECRET_COLOR,
            pinned_archive_avatar: ArchiveAvatarColors {
                background: Gradient {
                    top: Color::new(0x72, 0xd5, 0xfd, 0xff),
                    bottom: Color::new(0x2a, 0x9e, 0xf1, 0xff),
                },
                foreground: Color::WHITE,
            },
            unpinned_archive_avatar: ArchiveAvatarColors {
                background: Gradient {
                    top: Color::new(0xde, 0xde, 0xe5, 0xff),
                    bottom: Color::new(0xc5, 0xc6, 0xcc, 0xff),
                },
                foreground: Color::WHITE,
            },
            online_dot: Color::new(0x4c, 0xc9, 0x1f, 0xff),
        },
        chat: Chat {
            default_wallpaper: Wallpaper::Color(Color::WHITE),
            message: Message {
                incoming: PartedColors {
                    bubble: Bubble {
                        with_wp: Components {
                            bg: Color::WHITE,
                            highlighted_bg: Color::new(0xda, 0xda, 0xde, 0xff),
                            stroke: Color::WHITE,
                        },
                        without_wp: Components {
                            bg: Color::new(0xf1, 0xf1, 0xf4, 0xff),
                            highlighted_bg: Color::new(0xda, 0xda, 0xde, 0xff),
                            stroke: Color::new(0xf1, 0xf1, 0xf4, 0xff),
                        },
                    },
                    primary_text: Color::BLACK,
                    secondary_text: Color::new(0x52, 0x52, 0x52, 153),
                    link_text: Color::new(0x00, 0x4b, 0xad, 0xff),
                    link_highlight: Color {
                        alpha: 0x4c,
                        ..accent_color
                    },
                    scam: DESTRUCTIVE_COLOR,
                    text_highlight: Color::new(0xff, 0xe4, 0x38, 0xff),
                    accent_text: accent_color,
                    accent_control: accent_color,
                    media_active_control: accent_color,
                    media_inactive_control: Color::new(0xca, 0xca, 0xca, 0xff),
                    pending_activity: Color::new(0x52, 0x52, 0x52, 153),
                    file_title: accent_color,
                    file_description: Color::new(0x99, 0x99, 0x99, 0xff),
                    file_duration: Color::new(0x52, 0x52, 0x52, 153),
                    media_placeholder: Color::new(0xf2, 0xf2, 0xf2, 0xff),
                    polls: Polls {
                        radio_button: Color::new(0xc8, 0xc7, 0xcc, 0xff),
                        radio_progress: accent_color,
                        highlight: Color {
                            alpha: 0x1e,
                            ..accent_color
                        },
                        separator: Color::new(0xc8, 0xc7, 0xcc, 0xff),
                        bar: accent_color,
                    },
                    action_buttons_bg: VariableColor {
                        with_wp: service_background_color,
                        without_wp: Color::new(0xff, 0xff, 0xff, 204),
                    },
                    action_buttons_stroke: VariableColor {
                        with_wp: Color::CLEAR,
                        without_wp: accent_color,
                    },
                    action_buttons_text: VariableColor {
                        with_wp: Color::WHITE,
                        without_wp: accent_color,
                    },
                    text_selection: Color {
                        alpha: 0x4c,
                        ..accent_color
                    },
                    text_selection_knob: accent_color,
                },
                outgoing: PartedColors {
                    bubble: Bubble {
                        with_wp: Components {
                            bg: outgoing_bubble_background,
                            highlighted_bg: {
                                let (hue, saturation, brightness) =
                                    outgoing_bubble_background.hsb();

                                Color::from_hsb(
                                    hue,
                                    saturation,
                                    brightness * 0.7,
                                    outgoing_bubble_background.alpha,
                                )
                            },
                            stroke: outgoing_bubble_stroke,
                        },
                        without_wp: Components {
                            bg: outgoing_bubble_background,
                            highlighted_bg: {
                                let (hue, saturation, brightness) =
                                    outgoing_bubble_background.hsb();

                                Color::from_hsb(
                                    hue,
                                    saturation,
                                    brightness * 0.7,
                                    outgoing_bubble_background.alpha,
                                )
                            },
                            stroke: outgoing_bubble_stroke,
                        },
                    },
                    primary_text: outgoing_primary_text,
                    secondary_text: outgoing_secondary_text,
                    link_text: outgoing_link,
                    link_highlight: Color::new(0xff, 0xff, 0xff, 0x4c),
                    scam: outgoing_primary_text,
                    text_highlight: Color::new(0xff, 0xe4, 0x38, 0xff),
                    accent_text: outgoing_primary_text,
                    accent_control: outgoing_primary_text,
                    media_active_control: outgoing_primary_text,
                    media_inactive_control: outgoing_secondary_text,
                    pending_activity: outgoing_secondary_text,
                    file_title: outgoing_primary_text,
                    file_description: outgoing_secondary_text,
                    file_duration: outgoing_secondary_text,
                    media_placeholder: {
                        let (hue, saturation, brightness) = accent_color.hsb();
                        Color::from_hsb(
                            hue,
                            saturation,
                            brightness * 0.95,
                            accent_color.alpha,
                        )
                    },
                    polls: Polls {
                        radio_button: outgoing_secondary_text,
                        radio_progress: outgoing_primary_text,
                        highlight: Color {
                            alpha: 0x1e,
                            ..outgoing_primary_text
                        },
                        separator: outgoing_secondary_text,
                        bar: outgoing_primary_text,
                    },
                    action_buttons_bg: VariableColor {
                        with_wp: service_background_color,
                        without_wp: Color::new(0xff, 0xff, 0xff, 204),
                    },
                    action_buttons_stroke: VariableColor {
                        with_wp: Color::CLEAR,
                        without_wp: accent_color,
                    },
                    action_buttons_text: VariableColor {
                        with_wp: Color::WHITE,
                        without_wp: accent_color,
                    },
                    text_selection: Color {
                        alpha: 51,
                        ..outgoing_selection
                    },
                    text_selection_knob: outgoing_selection,
                },
                freeform: Bubble {
                    with_wp: Components {
                        bg: Color::new(0xe5, 0xe5, 0xea, 0xff),
                        highlighted_bg: Color::new(0xda, 0xda, 0xde, 0xff),
                        stroke: Color::new(0xe5, 0xe5, 0xea, 0xff),
                    },
                    without_wp: Components {
                        bg: Color::new(0xe5, 0xe5, 0xea, 0xff),
                        highlighted_bg: Color::new(0xda, 0xda, 0xde, 0xff),
                        stroke: Color::new(0xe5, 0xe5, 0xea, 0xff),
                    },
                },
                info_primary_text: Color::BLACK,
                info_link_text: Color::new(0x00, 0x4b, 0xad, 0xff),
                outgoing_check,
                media_date_and_status_bg: Color::new(0x00, 0x00, 0x00, 0x7f),
                media_date_and_status_text: Color::WHITE,
                share_button_bg: VariableColor {
                    with_wp: service_background_color,
                    without_wp: Color::new(0xff, 0xff, 0xff, 204),
                },
                share_button_stroke: VariableColor {
                    with_wp: Color::CLEAR,
                    without_wp: Color::new(0xe5, 0xe5, 0xea, 0xff),
                },
                share_button_fg: VariableColor {
                    with_wp: Color::WHITE,
                    without_wp: accent_color,
                },
                media_overlay_control: FillForeground {
                    bg: Color::new(0x00, 0x00, 0x00, 153),
                    fg: Color::WHITE,
                },
                selection_control: FillStrokeForeground {
                    bg: accent_color,
                    stroke: Color::new(0xc7, 0xc7, 0xcc, 0xff),
                    fg: Color::WHITE,
                },
                delivery_failed: FillForeground {
                    bg: DESTRUCTIVE_COLOR,
                    fg: Color::WHITE,
                },
                media_highlight_overlay: Color::new(0xff, 0xff, 0xff, 153),
            },
            service_message: ServiceMessage {
                components: service_message::Color {
                    with_default_wp: service_message::color::Components {
                        bg: Color::new(0xff, 0xff, 0xff, 204),
                        primary_text: Color::new(0x8d, 0x8e, 0x93, 0xff),
                        link_highlight: Color::new(0x74, 0x83, 0x91, 0x3f),
                        scam: DESTRUCTIVE_COLOR,
                        date_fill_static: Color::new(0xff, 0xff, 0xff, 204),
                        date_fill_float: Color::new(0xff, 0xff, 0xff, 204),
                    },
                    with_custom_wp: service_message::color::Components {
                        bg: service_background_color,
                        primary_text: Color::WHITE,
                        link_highlight: Color::new(0x74, 0x83, 0x91, 0x3f),
                        scam: DESTRUCTIVE_COLOR,
                        date_fill_static: service_background_color,
                        date_fill_float: Color {
                            alpha: (service_background_color.alpha as f64 * 2.0
                                / 3.0) as u8,
                            ..service_background_color
                        },
                    },
                },
                unread_bar_bg: Color::WHITE,
                unread_bar_stroke: Color::WHITE,
                unread_bar_text: Color::new(0x8d, 0x8e, 0x93, 0xff),
                date_text: VariableColor {
                    with_wp: Color::WHITE,
                    without_wp: Color::new(0x8d, 0x8e, 0x93, 0xff),
                },
            },
            input_panel: InputPanel {
                panel_bg: Color::new(0xf7, 0xf7, 0xf7, 0xff),
                panel_separator: Color::new(0xb2, 0xb2, 0xb2, 0xff),
                panel_control_accent: accent_color,
                panel_control: Color::new(0x85, 0x8e, 0x99, 0xff),
                panel_control_disabled: Color::new(0x72, 0x7b, 0x87, 0x7f),
                panel_control_destructive: Color::new(0xff, 0x3b, 0x30, 0xff),
                input_bg: Color::WHITE,
                input_stroke: Color::new(0xd9, 0xdc, 0xdf, 0xff),
                input_placeholder: Color::new(0xbe, 0xbe, 0xc0, 0xff),
                input_text: Color::BLACK,
                input_control: Color::new(0xa0, 0xa7, 0xb0, 0xff),
                action_control_bg: accent_color,
                action_control_fg: Color::WHITE,
                primary_text: Color::BLACK,
                secondary_text: Color::new(0x8e, 0x8e, 0x93, 0xff),
                media_record_dot: Color::new(0xed, 0x25, 0x21, 0xff),
                media_record_control: MediaRecordControl {
                    button: accent_color,
                    mic_level: Color {
                        alpha: 51,
                        ..accent_color
                    },
                    active_icon: Color::WHITE,
                },
            },
            input_media_panel: InputMediaPanel {
                panel_separator: Color::new(0xbe, 0xc2, 0xc6, 0xff),
                panel_icon: Color::new(0x85, 0x8e, 0x99, 0xff),
                panel_highlighted_icon_bg: Color::new(0x85, 0x8e, 0x99, 51),
                stickers_bg: Color::new(0xe8, 0xeb, 0xf0, 0xff),
                stickers_section_text: Color::new(0x90, 0x99, 0xa2, 0xff),
                stickers_search_bg: Color::new(0xd9, 0xdb, 0xe1, 0xff),
                stickers_search_placeholder: Color::new(0x8e, 0x8e, 0x93, 0xff),
                stickers_search_primary: Color::BLACK,
                stickers_search_control: Color::new(0x8e, 0x8e, 0x93, 0xff),
                gifs_bg: Color::WHITE,
            },
            input_button_panel: InputButtonPanel {
                panel_separator: Color::new(0xbe, 0xc2, 0xc6, 0xff),
                panel_bg: Color::new(0xde, 0xe2, 0xe6, 0xff),
                button_bg: Color::WHITE,
                button_stroke: Color::new(0xc3, 0xc7, 0xc9, 0xff),
                button_highlighted_bg: Color::new(0xa8, 0xb3, 0xc0, 0xff),
                button_highlighted_stroke: Color::new(0xc3, 0xc7, 0xc9, 0xff),
                button_text: Color::BLACK,
            },
            history_nav: HistoryNav {
                bg: Color::new(0xf7, 0xf7, 0xf7, 0xff),
                stroke: Color::new(0xb1, 0xb1, 0xb1, 0xff),
                fg: Color::new(0x88, 0x88, 0x8d, 0xff),
                badge_bg: accent_color,
                badge_stroke: accent_color,
                badge_text: Color::WHITE,
            },
        },
        action_sheet: ActionSheet {
            dim: Color::new(0, 0, 0, 102),
            bg_type: action_sheet::BgType::Light,
            opaque_item_bg: Color::WHITE,
            item_bg: Color::new(255, 255, 255, 0xdd),
            opaque_item_highlighted_bg: Color::new(0xe5, 0xe5, 0xe5, 255),
            item_highlighted_bg: Color::new(0xe5, 0xe5, 0xe5, 0xb2),
            opaque_item_separator: Color::new(0xe5, 0xe5, 0xe5, 255),
            standard_action_text: accent_color,
            destructive_action_text: DESTRUCTIVE_COLOR,
            disabled_action_text: Color::new(0xb3, 0xb3, 0xb3, 0xff),
            primary_text: Color::BLACK,
            secondary_text: Color::new(0x5e, 0x5e, 0x5e, 0xff),
            control_accent: accent_color,
            input_bg: Color::new(0xe9, 0xe9, 0xe9, 0xff),
            input_hollow_bg: Color::WHITE,
            input_border: Color::new(0xe4, 0xe4, 0xe6, 0xff),
            input_placeholder: Color::new(0x81, 0x80, 0x86, 0xff),
            input_text: Color::BLACK,
            input_clear_button: Color::new(0x7b, 0x7b, 0x81, 0xff),
            check_content: Color::WHITE,
        },
        context_menu: ContextMenu {
            dim: Color::new(0x00, 0x0a, 0x26, 51),
            background: Color::new(0xf9, 0xf9, 0xf9, 0xc6),
            item_separator: Color::new(0x3c, 0x3c, 0x43, 51),
            section_separator: Color::new(0x8a, 0x8a, 0x8a, 51),
            item_bg: Color::CLEAR,
            item_highlighted_bg: Color::new(0x3c, 0x3c, 0x43, 51),
            primary: Color::BLACK,
            secondary: Color::new(0x00, 0x00, 0x00, 204),
            destructive: DESTRUCTIVE_COLOR,
        },
        notification: Notification {
            bg: Color::WHITE,
            primary_text: Color::BLACK,
            expanded: Expanded {
                bg_type: expanded::BgType::Light,
                nav_bar: expanded::NavBar {
                    background: Color::WHITE,
                    primary_text: Color::BLACK,
                    control: Color::new(0x7e, 0x87, 0x91, 0xff),
                    separator: Color::new(0xb1, 0xb1, 0xb1, 0xff),
                },
            },
        },
    }
}
