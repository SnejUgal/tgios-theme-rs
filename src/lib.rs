//! Telegram iOS theme parser and serializer.

#![deny(
    future_incompatible,
    nonstandard_style,
    missing_docs,
    clippy::all,
    clippy::pedantic,
    clippy::nursery,
    clippy::cargo
)]

pub mod default_themes;
mod serializer;
pub mod theme;

pub use theme::Theme;
