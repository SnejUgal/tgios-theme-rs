use crate::theme::Color;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct FillForeground {
    pub bg: Color,
    pub fg: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct FillStrokeForeground {
    pub bg: Color,
    pub stroke: Color,
    pub fg: Color,
}
