use crate::theme::Color;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum BgType {
    Light,
    Dark,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct NavBar {
    pub background: Color,
    pub primary_text: Color,
    pub control: Color,
    pub separator: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Expanded {
    pub bg_type: BgType,
    pub nav_bar: NavBar,
}
