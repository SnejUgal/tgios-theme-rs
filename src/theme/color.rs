use serde::{
    de::{self, Deserialize, Deserializer, Visitor},
    ser::{Serialize, Serializer},
};
use std::fmt::{self, Formatter};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub struct Color {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
    pub alpha: u8,
}

impl Color {
    pub const CLEAR: Self = Self {
        red: 0,
        green: 0,
        blue: 0,
        alpha: 0,
    };

    pub const WHITE: Self = Self {
        red: 255,
        green: 255,
        blue: 255,
        alpha: 255,
    };

    pub const BLACK: Self = Self {
        red: 0,
        green: 0,
        blue: 0,
        alpha: 255,
    };

    pub const fn new(red: u8, green: u8, blue: u8, alpha: u8) -> Self {
        Self {
            red,
            green,
            blue,
            alpha,
        }
    }

    pub const fn from_rgba(rgba: u32) -> Self {
        Self {
            red: ((rgba & 0xff_00_00_00) >> (8 * 3)) as u8,
            green: ((rgba & 0x00_ff_00_00) >> (8 * 2)) as u8,
            blue: ((rgba & 0x00_00_ff_00) >> 8) as u8,
            alpha: (rgba & 0x00_00_00_ff) as u8,
        }
    }

    pub const fn from_tuple(
        (red, green, blue, alpha): (u8, u8, u8, u8),
    ) -> Self {
        Self {
            red,
            green,
            blue,
            alpha,
        }
    }

    pub const fn from_array([red, green, blue, alpha]: [u8; 4]) -> Self {
        Self {
            red,
            green,
            blue,
            alpha,
        }
    }

    pub const fn into_rgba(self) -> u32 {
        (self.red as u32) << (8 * 3)
            | (self.green as u32) << (8 * 2)
            | (self.blue as u32) << 8
            | (self.alpha as u32)
    }

    pub const fn into_tuple(self) -> (u8, u8, u8, u8) {
        (self.red, self.green, self.blue, self.alpha)
    }

    pub const fn into_array(self) -> [u8; 4] {
        [self.red, self.green, self.blue, self.alpha]
    }

    pub(crate) fn lightness(self) -> f64 {
        0.2126 * self.red as f64 / 255.0
            + 0.7152 * self.green as f64 / 255.0
            + 0.0722 * self.blue as f64 / 255.0
    }

    pub(crate) fn hsb(self) -> (f64, f64, f64) {
        let red = self.red as f64 / 255.0;
        let green = self.green as f64 / 255.0;
        let blue = self.blue as f64 / 255.0;
        let max = red.max(green).max(blue);
        let min = red.min(green).min(blue);
        let delta = max - min;

        let hue = if delta == 0.0 {
            0.0
        } else if max == red {
            60.0 * (green - blue) / delta
        } else if max == green {
            60.0 * (2.0 + (blue - red) / delta)
        } else {
            // if max == blue
            60.0 * (4.0 + (red - green) / delta)
        };

        let hue = if hue < 0.0 { hue + 360.0 } else { hue };
        let saturation = if max == 0.0 { 0.0 } else { delta / max };
        let brightness = max;

        (hue, saturation, brightness)
    }

    pub(crate) fn from_hsb(
        hue: f64,
        saturation: f64,
        brightness: f64,
        alpha: u8,
    ) -> Self {
        let chroma = brightness * saturation;
        let x = chroma * (1.0 - ((hue / 60.0) % 2.0 - 1.0)).abs();
        let m = brightness - chroma;

        let (red, green, blue) = if (0.0..60.0).contains(&hue) {
            (chroma, x, 0.0)
        } else if (60.0..120.0).contains(&hue) {
            (x, chroma, 0.0)
        } else if (120.0..180.0).contains(&hue) {
            (0.0, chroma, x)
        } else if (180.0..240.0).contains(&hue) {
            (0.0, x, chroma)
        } else if (240.0..300.0).contains(&hue) {
            (x, 0.0, chroma)
        } else {
            // if (300.0..360.0).contains(hue)
            (chroma, 0.0, x)
        };

        let red = ((red + m) * 255.0) as u8;
        let green = ((green + m) * 255.0) as u8;
        let blue = ((blue + m) * 255.0) as u8;

        Self {
            red,
            green,
            blue,
            alpha,
        }
    }

    pub(crate) fn parse(string: &str) -> Result<Self, std::num::ParseIntError> {
        if string.eq_ignore_ascii_case("clear") {
            return Ok(Color::CLEAR);
        }

        let start = if string.starts_with("#") { 1 } else { 0 };
        let color_part = &string[start..];
        let integer = u32::from_str_radix(color_part, 16)?;

        // This is how Telegram iOS actually parses the string:
        // https://github.com/TelegramMessenger/Telegram-iOS/blob/97eca304f8cee20fad75185bce2189cdb3e98005/submodules/Display/Display/UIKitUtils.swift#L54-L69
        let red = ((integer & 0x00_ff_00_00) >> (8 * 2)) as u8;
        let green = ((integer & 0x00_00_ff_00) >> 8) as u8;
        let blue = (integer & 0x00_00_00_ff) as u8;

        let alpha = if color_part.len() > 7 {
            ((integer & 0xff_00_00_00) >> (8 * 3)) as u8
        } else {
            255
        };

        Ok(Color {
            red,
            green,
            blue,
            alpha,
        })
    }
}

impl Serialize for Color {
    fn serialize<S: Serializer>(
        &self,
        serializer: S,
    ) -> Result<S::Ok, S::Error> {
        if *self == Color::CLEAR {
            return serializer.serialize_str("clear");
        }

        let color = if self.alpha == 255 {
            format!(
                "{red:0>2x}{green:0>2x}{blue:0>2x}",
                red = self.red,
                green = self.green,
                blue = self.blue,
            )
        } else {
            format!(
                "{alpha:0>2x}{red:0>2x}{green:0>2x}{blue:0>2x}",
                alpha = self.alpha,
                red = self.red,
                green = self.green,
                blue = self.blue,
            )
        };

        serializer.serialize_str(&color)
    }
}

struct ColorVisitor;

impl<'v> Visitor<'v> for ColorVisitor {
    type Value = Color;

    fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
        formatter.write_str("a valid `[aa]rrggbb` color or `clear`")
    }

    fn visit_str<E: de::Error>(self, string: &str) -> Result<Self::Value, E> {
        Color::parse(string).map_err(|_| de::Error::custom("Invalid Hex"))
    }
}

impl<'de> Deserialize<'de> for Color {
    fn deserialize<D: Deserializer<'de>>(
        deserializer: D,
    ) -> Result<Self, D::Error> {
        deserializer.deserialize_str(ColorVisitor)
    }
}
