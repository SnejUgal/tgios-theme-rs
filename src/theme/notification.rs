use crate::theme::Color;
use serde::{Deserialize, Serialize};

pub mod expanded;

pub use expanded::Expanded;

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Notification {
    pub bg: Color,
    pub primary_text: Color,
    pub expanded: Expanded,
}
