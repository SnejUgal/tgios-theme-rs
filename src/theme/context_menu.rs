use crate::theme::Color;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ContextMenu {
    pub dim: Color,
    pub background: Color,
    pub item_separator: Color,
    pub section_separator: Color,
    pub item_bg: Color,
    pub item_highlighted_bg: Color,
    pub primary: Color,
    pub secondary: Color,
    pub destructive: Color,
}
