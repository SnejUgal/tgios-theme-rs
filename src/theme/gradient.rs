use crate::theme::Color;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct Gradient {
    pub top: Color,
    pub bottom: Color,
}
