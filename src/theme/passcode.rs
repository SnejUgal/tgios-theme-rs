use crate::theme::{Color, Gradient};
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct Passcode {
    pub bg: Gradient,
    pub button: Color,
}
