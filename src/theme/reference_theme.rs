use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum ReferenceTheme {
    Day,
    Classic,
    NightTinted,
    Night,
}
