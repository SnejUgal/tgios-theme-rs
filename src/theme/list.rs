use crate::theme::{Color, FillForeground, FillStrokeForeground};
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct Switch {
    pub frame: Color,
    pub handle: Color,
    pub content: Color,
    pub positive: Color,
    pub negative: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct DisclosureActions {
    pub neutral1: FillForeground,
    pub neutral2: FillForeground,
    pub destructive: FillForeground,
    pub constructive: FillForeground,
    pub accent: FillForeground,
    pub warning: FillForeground,
    pub inactive: FillForeground,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub struct FreeInputField {
    pub bg: Color,
    pub stroke: Color,
    pub placeholder: Color,
    pub primary: Color,
    pub control: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct List {
    pub blocks_bg: Color,
    pub plain_bg: Color,
    pub primary_text: Color,
    pub secondary_text: Color,
    pub disabled_text: Color,
    pub accent: Color,
    pub highlighted: Color,
    pub destructive: Color,
    pub placeholder_text: Color,
    pub item_blocks_bg: Color,
    pub item_highlighted_bg: Color,
    pub blocks_separator: Color,
    pub plain_separator: Color,
    pub disclosure_arrow: Color,
    pub section_header_text: Color,
    pub free_text: Color,
    pub free_text_error: Color,
    pub free_text_success: Color,
    pub free_mono_icon: Color,
    pub switch: Switch,
    pub disclosure_actions: DisclosureActions,
    pub check: FillStrokeForeground,
    pub control_secondary: Color,
    pub free_input_field: FreeInputField,
    pub media_placeholder: Color,
    pub scroll_indicator: Color,
    pub page_indicator_inactive: Color,
    pub input_clear_button: Color,
}
