use crate::theme::Color;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum BgType {
    Light,
    Dark,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ActionSheet {
    pub dim: Color,
    pub bg_type: BgType,
    pub opaque_item_bg: Color,
    pub item_bg: Color,
    pub opaque_item_highlighted_bg: Color,
    pub item_highlighted_bg: Color,
    pub opaque_item_separator: Color,
    pub standard_action_text: Color,
    pub destructive_action_text: Color,
    pub disabled_action_text: Color,
    pub primary_text: Color,
    pub secondary_text: Color,
    pub control_accent: Color,
    pub input_bg: Color,
    pub input_hollow_bg: Color,
    pub input_border: Color,
    pub input_placeholder: Color,
    pub input_text: Color,
    pub input_clear_button: Color,
    pub check_content: Color,
}
