use crate::theme::{Color, StatusBar};
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Intro {
    pub status_bar: StatusBar,
    pub start_button: Color,
    pub dot: Color,
}
