use crate::theme::Color;
use serde::{Deserialize, Serialize};

pub mod input_panel;
pub mod message;
pub mod service_message;
pub mod wallpaper;

pub use {
    input_panel::InputPanel, message::Message, service_message::ServiceMessage,
    wallpaper::Wallpaper,
};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InputMediaPanel {
    pub panel_separator: Color,
    pub panel_icon: Color,
    pub panel_highlighted_icon_bg: Color,
    pub stickers_bg: Color,
    pub stickers_section_text: Color,
    pub stickers_search_bg: Color,
    pub stickers_search_placeholder: Color,
    pub stickers_search_primary: Color,
    pub stickers_search_control: Color,
    pub gifs_bg: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InputButtonPanel {
    pub panel_bg: Color,
    pub panel_separator: Color,
    pub button_bg: Color,
    pub button_stroke: Color,
    pub button_highlighted_bg: Color,
    pub button_highlighted_stroke: Color,
    pub button_text: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct HistoryNav {
    pub bg: Color,
    pub stroke: Color,
    pub fg: Color,
    pub badge_bg: Color,
    pub badge_stroke: Color,
    pub badge_text: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Chat {
    pub default_wallpaper: Wallpaper,
    pub message: Message,
    pub service_message: ServiceMessage,
    pub input_panel: InputPanel,
    pub input_media_panel: InputMediaPanel,
    pub input_button_panel: InputButtonPanel,
    pub history_nav: HistoryNav,
}
