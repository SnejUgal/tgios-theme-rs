use crate::theme::{Color, StatusBar};
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TabBar {
    pub background: Color,
    pub separator: Color,
    pub icon: Color,
    pub selected_icon: Color,
    pub text: Color,
    pub selected_text: Color,
    pub badge_background: Color,
    pub badge_stroke: Color,
    pub badge_text: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct NavBar {
    pub button: Color,
    pub disabled_button: Color,
    pub primary_text: Color,
    pub secondary_text: Color,
    pub control: Color,
    pub accent_text: Color,
    pub background: Color,
    pub separator: Color,
    pub badge_fill: Color,
    pub badge_stroke: Color,
    pub badge_text: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SearchBar {
    pub background: Color,
    pub accent: Color,
    pub input_fill: Color,
    pub input_text: Color,
    pub input_placeholder_text: Color,
    pub input_icon: Color,
    pub input_clear_button: Color,
    pub separator: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum Keyboard {
    Light,
    Dark,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Root {
    pub status_bar: StatusBar,
    pub tab_bar: TabBar,
    pub nav_bar: NavBar,
    pub search_bar: SearchBar,
    pub keyboard: Keyboard,
}
