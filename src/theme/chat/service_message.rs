use crate::theme::{self, VariableColor};
use serde::{Deserialize, Serialize};

pub mod color;

pub use color::Color;

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ServiceMessage {
    pub components: Color,
    pub unread_bar_bg: theme::Color,
    pub unread_bar_stroke: theme::Color,
    pub unread_bar_text: theme::Color,
    pub date_text: VariableColor,
}
