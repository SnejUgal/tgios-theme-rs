use crate::theme::Color;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct MediaRecordControl {
    pub button: Color,
    pub mic_level: Color,
    pub active_icon: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InputPanel {
    pub panel_bg: Color,
    pub panel_separator: Color,
    pub panel_control_accent: Color,
    pub panel_control: Color,
    pub panel_control_disabled: Color,
    pub panel_control_destructive: Color,
    pub input_bg: Color,
    pub input_stroke: Color,
    pub input_placeholder: Color,
    pub input_text: Color,
    pub input_control: Color,
    pub action_control_bg: Color,
    pub action_control_fg: Color,
    pub primary_text: Color,
    pub secondary_text: Color,
    pub media_record_dot: Color,
    pub media_record_control: MediaRecordControl,
}
