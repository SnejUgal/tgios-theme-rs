use crate::theme::Color;
use serde::{
    de::{self, Deserialize, Deserializer, Visitor},
    ser::{Serialize, Serializer},
};
use std::fmt::{self, Formatter};

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub enum Wallpaper {
    BuiltIn,
    Color(Color),
    Remote {
        slug: String,
        color: Option<Color>,
        intensity: Option<u8>,
        is_blur: bool,
        is_motion: bool,
    },
}

impl Serialize for Wallpaper {
    fn serialize<S: Serializer>(
        &self,
        serializer: S,
    ) -> Result<S::Ok, S::Error> {
        match self {
            Wallpaper::BuiltIn => serializer.serialize_str("builtin"),
            Wallpaper::Color(color) => color.serialize(serializer),
            Wallpaper::Remote {
                slug,
                color,
                intensity,
                is_blur,
                is_motion,
            } => {
                let mut string = slug.clone();

                if let Some(color) = color {
                    string.push(' ');
                    string += &format!(
                        "{red:0>2x}{green:0>2x}{blue:0>2x}",
                        red = color.red,
                        green = color.green,
                        blue = color.blue,
                    );
                }

                if let Some(intensity) = intensity {
                    string.push(' ');
                    string += &intensity.to_string();
                }

                if *is_motion {
                    string += " motion";
                }

                if *is_blur {
                    string += " blur";
                }

                serializer.serialize_str(&string)
            }
        }
    }
}

struct WallpaperVisitor;

impl<'v> Visitor<'v> for WallpaperVisitor {
    type Value = Wallpaper;

    fn expecting(&self, formatter: &mut Formatter) -> fmt::Result {
        formatter.write_str("a valid wallpaper syntax")
    }

    fn visit_str<E: de::Error>(self, string: &str) -> Result<Self::Value, E> {
        if string.eq_ignore_ascii_case("builtin") {
            return Ok(Wallpaper::BuiltIn);
        }

        if [6, 7].contains(&string.len()) {
            let color = Color::parse(string)
                .map_err(|_| de::Error::custom("Invalid color"))?;

            return Ok(Wallpaper::Color(color));
        }

        let mut components = string.split(' ');
        let mut is_blur = false;
        let mut is_motion = false;
        let slug = components
            .next()
            .ok_or_else(|| de::Error::custom("missing slug"))?;

        // Trying to be as close to the original parser as possible
        if slug == "blur" {
            is_blur = true;
        } else if slug == "motion" {
            is_motion = true;
        }

        let color = if let Some(color) = components.next() {
            if color == "blur" {
                is_blur = true;
                None
            } else if color == "motion" {
                is_motion = true;
                None
            } else if color.len() == 6 {
                if let Ok(color) = Color::parse(color) {
                    Some(color)
                } else {
                    None
                }
            } else {
                None
            }
        } else {
            None
        };

        let intensity = if let Some(intensity) = components.next() {
            if intensity == "blur" {
                is_blur = true;
                None
            } else if intensity == "motion" {
                is_motion = true;
                None
            } else if let Ok(intensity) = intensity.parse::<i32>() {
                Some(if (0..=100).contains(&intensity) {
                    intensity as u8
                } else {
                    50
                })
            } else {
                None
            }
        } else {
            None
        };

        for component in components {
            if component == "blur" {
                is_blur = true;
            } else if component == "motion" {
                is_motion = true;
            }
        }

        Ok(Wallpaper::Remote {
            slug: slug.to_string(),
            color,
            intensity,
            is_blur,
            is_motion,
        })
    }
}

impl<'de> Deserialize<'de> for Wallpaper {
    fn deserialize<D: Deserializer<'de>>(
        deserializer: D,
    ) -> Result<Self, D::Error> {
        deserializer.deserialize_str(WallpaperVisitor)
    }
}
