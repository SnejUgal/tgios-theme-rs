use crate::theme::{
    Color, FillForeground, FillStrokeForeground, VariableColor,
};
use serde::{Deserialize, Serialize};

pub mod bubble;
pub mod parted_colors;

pub use {bubble::Bubble, parted_colors::PartedColors};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Message {
    pub incoming: PartedColors,
    pub outgoing: PartedColors,
    pub freeform: Bubble,
    pub info_primary_text: Color,
    pub info_link_text: Color,
    pub outgoing_check: Color,
    pub media_date_and_status_bg: Color,
    pub media_date_and_status_text: Color,
    pub share_button_bg: VariableColor,
    pub share_button_stroke: VariableColor,
    pub share_button_fg: VariableColor,
    pub media_overlay_control: FillForeground,
    pub selection_control: FillStrokeForeground,
    pub delivery_failed: FillForeground,
    pub media_highlight_overlay: Color,
}
