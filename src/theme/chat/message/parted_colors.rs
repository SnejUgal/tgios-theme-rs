use crate::theme::{chat::message::Bubble, Color, VariableColor};
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Polls {
    pub radio_button: Color,
    pub radio_progress: Color,
    pub highlight: Color,
    pub separator: Color,
    pub bar: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PartedColors {
    pub bubble: Bubble,
    pub primary_text: Color,
    pub secondary_text: Color,
    pub link_text: Color,
    pub link_highlight: Color,
    pub scam: Color,
    pub text_highlight: Color,
    pub accent_text: Color,
    pub accent_control: Color,
    pub media_active_control: Color,
    pub media_inactive_control: Color,
    pub pending_activity: Color,
    pub file_title: Color,
    pub file_description: Color,
    pub file_duration: Color,
    pub media_placeholder: Color,
    pub polls: Polls,
    pub action_buttons_bg: VariableColor,
    pub action_buttons_stroke: VariableColor,
    pub action_buttons_text: VariableColor,
    pub text_selection: Color,
    pub text_selection_knob: Color,
}
