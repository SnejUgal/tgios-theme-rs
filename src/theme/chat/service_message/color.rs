use crate::theme;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Components {
    pub bg: theme::Color,
    pub primary_text: theme::Color,
    pub link_highlight: theme::Color,
    pub scam: theme::Color,
    pub date_fill_static: theme::Color,
    pub date_fill_float: theme::Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Color {
    pub with_default_wp: Components,
    pub with_custom_wp: Components,
}
