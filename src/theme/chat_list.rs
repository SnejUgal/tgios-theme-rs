use crate::theme::{Color, Gradient};
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ArchiveAvatarColors {
    pub background: Gradient,
    pub foreground: Color,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ChatList {
    pub bg: Color,
    pub item_separator: Color,
    pub item_bg: Color,
    pub pinned_item_bg: Color,
    pub item_highlighted_bg: Color,
    pub item_selected_bg: Color,
    pub title: Color,
    pub secret_title: Color,
    pub date_text: Color,
    pub author_name: Color,
    pub message_text: Color,
    pub message_draft_text: Color,
    pub checkmark: Color,
    pub pending_indicator: Color,
    pub failed_fill: Color,
    pub failed_fg: Color,
    pub mute_icon: Color,
    pub unread_badge_active_bg: Color,
    pub unread_badge_active_text: Color,
    pub unread_badge_inactive_bg: Color,
    pub unread_badge_inactive_text: Color,
    pub pinned_badge: Color,
    pub pinned_search_bar: Color,
    pub regular_search_bar: Color,
    pub section_header_bg: Color,
    pub section_header_text: Color,
    pub verified_icon_bg: Color,
    pub verified_icon_fg: Color,
    pub secret_icon: Color,
    pub pinned_archive_avatar: ArchiveAvatarColors,
    pub unpinned_archive_avatar: ArchiveAvatarColors,
    pub online_dot: Color,
}
