use crate::Theme;
use serde::ser::{
    self, Serialize, SerializeMap, SerializeSeq, SerializeStruct,
    SerializeStructVariant, SerializeTuple, SerializeTupleStruct,
    SerializeTupleVariant, Serializer,
};
use std::{
    error::Error,
    fmt::{self, Display, Formatter},
    iter::repeat,
};

pub fn serialize(theme: &Theme) -> String {
    let mut serializer = ThemeSerializer {
        output: String::new(),
        // the first value to serialize is a struct, and it will increase this
        // value to 0
        nesting_level: -1,
    };
    theme.serialize(&mut serializer).unwrap();
    serializer.output.push('\n');
    serializer.output
}

fn fail() -> ! {
    unreachable!(
        "Tried to serialize an invalid type in tgios_theme::ThemeSerializer",
    )
}

struct ThemeSerializer {
    output: String,
    nesting_level: i8,
}

#[derive(Debug)]
enum Never {}

impl Display for Never {
    fn fmt(&self, _: &mut Formatter) -> fmt::Result {
        match *self {}
    }
}

impl Error for Never {}
impl ser::Error for Never {
    fn custom<T: Display>(_: T) -> Self {
        unreachable!()
    }
}

impl SerializeSeq for Never {
    type Ok = ();
    type Error = Self;

    fn serialize_element<T: ?Sized>(
        &mut self,
        _: &T,
    ) -> Result<(), Self::Error> {
        match *self {}
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        match self {}
    }
}

impl SerializeTuple for Never {
    type Ok = ();
    type Error = Self;

    fn serialize_element<T: ?Sized>(
        &mut self,
        _: &T,
    ) -> Result<(), Self::Error> {
        match *self {}
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        match self {}
    }
}

impl SerializeTupleStruct for Never {
    type Ok = ();
    type Error = Self;

    fn serialize_field<T: ?Sized>(&mut self, _: &T) -> Result<(), Self::Error> {
        match *self {}
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        match self {}
    }
}

impl SerializeTupleVariant for Never {
    type Ok = ();
    type Error = Self;

    fn serialize_field<T: ?Sized>(&mut self, _: &T) -> Result<(), Self::Error> {
        match *self {}
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        match self {}
    }
}

impl SerializeMap for Never {
    type Ok = ();
    type Error = Self;

    fn serialize_key<T: ?Sized>(&mut self, _: &T) -> Result<(), Self::Error> {
        match *self {}
    }

    fn serialize_value<T: ?Sized>(&mut self, _: &T) -> Result<(), Self::Error> {
        match *self {}
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        match self {}
    }
}

impl SerializeStructVariant for Never {
    type Ok = ();
    type Error = Self;

    fn serialize_field<T: ?Sized>(
        &mut self,
        _: &'static str,
        _: &T,
    ) -> Result<(), Self::Error> {
        match *self {}
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        match self {}
    }
}

impl Serializer for &'_ mut ThemeSerializer {
    type Ok = ();
    type Error = Never;

    type SerializeSeq = Never;
    type SerializeTuple = Never;
    type SerializeTupleStruct = Never;
    type SerializeTupleVariant = Never;
    type SerializeMap = Never;
    type SerializeStruct = Self;
    type SerializeStructVariant = Never;

    fn serialize_bool(self, value: bool) -> Result<Self::Ok, Self::Error> {
        self.output.push(' ');
        self.output += if value { "true" } else { "false" };
        Ok(())
    }

    fn serialize_i8(self, _: i8) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_i16(self, _: i16) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_i32(self, _: i32) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_i64(self, _: i64) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_u8(self, _: u8) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_u16(self, _: u16) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_u32(self, _: u32) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_u64(self, _: u64) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_f32(self, _: f32) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_f64(self, _: f64) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_char(self, _: char) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_str(self, string: &str) -> Result<Self::Ok, Self::Error> {
        self.output.push(' ');
        self.output += string;
        Ok(())
    }

    fn serialize_bytes(self, _: &[u8]) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_none(self) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_some<T: ?Sized + Serialize>(
        self,
        value: &T,
    ) -> Result<Self::Ok, Self::Error> {
        value.serialize(self)
    }

    fn serialize_unit(self) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_unit_struct(
        self,
        _: &'static str,
    ) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_unit_variant(
        self,
        _: &'static str,
        _: u32,
        variant: &'static str,
    ) -> Result<Self::Ok, Self::Error> {
        self.output.push(' ');
        self.output += variant;
        Ok(())
    }

    fn serialize_newtype_struct<T: ?Sized>(
        self,
        _: &'static str,
        _: &T,
    ) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_newtype_variant<T: ?Sized>(
        self,
        _: &'static str,
        _: u32,
        _: &'static str,
        _: &T,
    ) -> Result<Self::Ok, Self::Error> {
        fail()
    }

    fn serialize_seq(
        self,
        _: Option<usize>,
    ) -> Result<Self::SerializeSeq, Self::Error> {
        fail()
    }

    fn serialize_tuple(
        self,
        _: usize,
    ) -> Result<Self::SerializeTuple, Self::Error> {
        fail()
    }

    fn serialize_tuple_struct(
        self,
        _: &'static str,
        _: usize,
    ) -> Result<Self::SerializeTupleStruct, Self::Error> {
        fail()
    }

    fn serialize_tuple_variant(
        self,
        _: &'static str,
        _: u32,
        _: &'static str,
        _: usize,
    ) -> Result<Self::SerializeTupleVariant, Self::Error> {
        fail()
    }

    fn serialize_map(
        self,
        _: Option<usize>,
    ) -> Result<Self::SerializeMap, Self::Error> {
        fail()
    }

    fn serialize_struct(
        self,
        _: &'static str,
        _: usize,
    ) -> Result<Self::SerializeStruct, Self::Error> {
        self.nesting_level += 1;
        Ok(self)
    }

    fn serialize_struct_variant(
        self,
        _: &'static str,
        _: u32,
        _: &'static str,
        _: usize,
    ) -> Result<Self::SerializeStructVariant, Self::Error> {
        fail()
    }
}

impl SerializeStruct for &'_ mut ThemeSerializer {
    type Ok = ();
    type Error = Never;

    fn serialize_field<T: ?Sized + Serialize>(
        &mut self,
        key: &'static str,
        value: &T,
    ) -> Result<(), Self::Error> {
        if !self.output.is_empty() {
            self.output.push('\n');
        }

        self.output
            .extend(repeat(' ').take(self.nesting_level as usize * 2));
        self.output += key;
        self.output.push(':');
        value.serialize(&mut **self)?;
        Ok(())
    }

    fn end(self) -> Result<Self::Ok, Self::Error> {
        self.nesting_level -= 1;
        Ok(())
    }
}
